<?php 
 header("Access-Control-Allow-Origin: *");

require_once('./utilities.php');

$data = new stdClass();

if( isset($_GET['type']) ){
    switch($_GET['type']){
        case 'single':
            if( !isset($_GET['unit'])){die;}
            $data = getApartment($_GET['unit']);
            break;
        default:
            die;
            break;
    }
}else{
    $data->lastUpdate = getLastUpdate();
    $data->floors = getFloorList();
    $data->layouts = getLayoutList();
    $data->buildings = getBuildingList();
    $data->apartments = getApartments();
}

echo json_encode($data);