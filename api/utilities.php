<?php
    //Setting up Database Connection
    $server = "localhost";
    require_once("db_config.php");
    $conn = new mysqli($server, $username, $password, $database);

    function getBuildingList(){
        $query = $GLOBALS['conn']->prepare('SELECT `number` FROM `building` ORDER BY `number` ASC');
        $query->execute();
        $query->bind_result($number);

        $buildings = [];
        while($query->fetch()){
            array_push($buildings, $number);
        }

        $query->close();
        return $buildings;
    }
    
    function getBuilding($id){
        $query = $GLOBALS['conn']->prepare('SELECT `number` FROM `building` WHERE `id` = ?');
        $query->bind_param("i",$id);
        $query->execute();
        $query->bind_result($number);
        $query->fetch();
        $query->close();
        return $number;
    }
    
    function getFloorList(){
        $query = $GLOBALS['conn']->prepare('SELECT DISTINCT `floor` FROM `apartment` ORDER BY `floor` ASC');
        $query->execute();
        $query->bind_result($floor);
        
        $floors = [];
        while($query->fetch()){
            array_push($floors, $floor);
        }
        
        $query->close();
        return $floors;
    }
    
    function getLayoutList(){
        $query = $GLOBALS['conn']->prepare('SELECT DISTINCT `name` FROM `layouts` ORDER BY `name` ASC');
        $query->execute();
        $query->bind_result($name);
    
        $names = [];
        while($query->fetch()){
            array_push($names, $name);
        }
    
        $query->close();
        return $names;
    }

    function getLayout($id){
        $query = $GLOBALS['conn']->prepare('SELECT `name`, `bed`, `bath`, `size` FROM `layouts` WHERE `id` = ?');
        $query->bind_param("i",$id);
        $query->execute();
        $query->bind_result($name, $bed, $bath, $size);
        $query->fetch();

        $l = new stdClass();
        $l->name = $name;
        $l->bed = $bed;
        $l->bath = $bath;
        $l->size = $size;
        
        
        $query->close();
        return $l;
    }
    
    function getPrices($id){
        $query = $GLOBALS['conn']->prepare('SELECT DISTINCT `price` FROM `price` WHERE `apt_id` = ? ORDER BY `timestamp` DESC LIMIT 30');
        $query->bind_param("i",$id);
        $query->execute();
        $query->bind_result($price);
        $prices = [];
        while($query->fetch()){
            $p = new stdClass();
            $p->amount = $price;
            array_push($prices, $p);
        }
        $query->close();
        return $prices;
    }

    function getAllPrices($id){
        $query = $GLOBALS['conn']->prepare('SELECT `price`, `timestamp` FROM `price` WHERE `apt_id` = ? ORDER BY `timestamp` DESC LIMIT 500');
        $query->bind_param("i",$id);
        $query->execute();
        $query->bind_result($price, $timestamp);
        $prices = [];
        while($query->fetch()){
            $p = new stdClass();
            $p->amount = $price;
            $p->timestamp = $timestamp;
            array_push($prices, $p);
        }
        $query->close();
        return $prices;
    }
    
    function getCurrentPrice($id){
        $query = $GLOBALS['conn']->prepare('SELECT `price` FROM `price` WHERE `apt_id` = ? ORDER BY `timestamp` DESC LIMIT 1');
        $query->bind_param("i",$id);
        $query->execute();
        $query->bind_result($price);
        $query->fetch();
        $query->close();
        return $price;
    }

    function getDayBeforePrice($id){
        $query = $GLOBALS['conn']->prepare('SELECT `price`, `timestamp` FROM price WHERE DAY(`timestamp`) = ( SELECT EXTRACT(DAY FROM DATE_SUB(`last_update`, INTERVAL 0 DAY)) FROM apartment WHERE `id` = ? ) AND `apt_id` = ? ORDER BY `timestamp` ASC LIMIT 1');
        $query->bind_param("ii",$id,$id);
        $query->execute();
        $query->bind_result($price, $timestamp);
        $query->fetch();
        $query->close();
        return $price;
    }

    function getAmenities($id){
        $query = $GLOBALS['conn']->prepare('SELECT `amenities`.`name` FROM `amenities` INNER JOIN `apt_amen` ON `amenities`.`id` = `amen_id` INNER JOIN `apartment` ON `apt_id` = `apartment`.`id` WHERE `apartment`.`id` = ?');
        $query->bind_param("i",$id);
        $query->execute();
        $query->bind_result($name);
        $amenities = [];
        while($query->fetch()){
            array_push($amenities, $name);
        }
        $query->close();
        return $amenities;
    }

    function getApartments(){
        $qApartments = $GLOBALS['conn']->prepare('SELECT * FROM `apartment` WHERE `available` = 1');
        $qApartments->execute();
        $qApartments->bind_result($id, $unit, $floor, $availDate, $buildingId, $layoutId, $avail, $lastUpdate);

        $apartments = [];
        while($qApartments->fetch()){
            $a = new stdClass();
            $a->id = $id;
            $a->unit = $unit;
            $a->floor = $floor;
            $a->availableDate = $availDate;
            $a->building = $buildingId;
            $a->layout = $layoutId;
            array_push($apartments, $a);
        }
        
        foreach($apartments as $a){
            $a->building = getBuilding($a->building);
            $a->layout = getLayout($a->layout);
            $a->currentPrice = getCurrentPrice($a->id);
            $a->dayBeforePrice = getDayBeforePrice($a->id);
            $a->prices = getPrices($a->id);
            $a->amenities = getAmenities($a->id);
        }

        return $apartments;
    }

    function getApartment($unit){
        $query = $GLOBALS['conn']->prepare('SELECT * FROM `apartment` WHERE `unit` = ?');
        $query->bind_param("i", $unit);
        $query->execute();
        $query->bind_result($id, $unit, $floor, $availDate, $buildingId, $layoutId, $avail, $lastUpdate);
        $query->fetch();
        $query->close();

        $a = new stdClass();
        $a->id = $id;
        $a->unit = $unit;
        $a->floor = $floor;
        $a->availableDate = $availDate;
        $a->building = $buildingId;
        $a->layout = $layoutId;
        $a->building = getBuilding($buildingId);
        $a->layout = getLayout($layoutId);
        $a->currentPrice = getCurrentPrice($id);
        $a->dayBeforePrice = getDayBeforePrice($id);
        $a->prices = getAllPrices($id);
        $a->amenities = getAmenities($id);

        return $a;
    }

    function getLastUpdate(){
        $query = $GLOBALS['conn']->prepare('SELECT `last_update` FROM `apartment` WHERE `available` = 1 LIMIT 1');
        $query->execute();
        $query->bind_result($lastUpdate);
        $query->fetch();
        $query->close();
        return $lastUpdate;
    }