<?php

//Setting up Database Connection
require_once("db_config.php");
$server = "localhost";
$conn = new mysqli($server, $username, $password, $database);

if( $conn->connect_error ){
    die("Connection failed: " . $conn->connect_error);
}

/*Requiring Scaper*/
require('simple_html_dom.php');

if( isset($_GET['live']) ){
    $html = file_get_html("https://www.equityapartments.com/alexandria/mark-center/town-square-at-mark-center-apartments");
}else{
    if( !file_exists("website.html") ) {
        /*Temp file does not exist*/
        $html = file_get_html("https://www.equityapartments.com/alexandria/mark-center/town-square-at-mark-center-apartments");
        file_put_contents("website.html", $html);
    }else{
        /*Temp file exists*/
        $html = file_get_html("website.html");
    }
}

/*Getting all apartment sections*/
$apartment_types = [];
foreach($html->find('div.bedroom-type-section') as $types) {
    array_push($apartment_types, $types);
}

/*Getting apartments*/
$apartments = [];
foreach($apartment_types[1]->find('li.list-group-item') as $apt){
    $apartment = new stdClass();
    $comment = $apt->find('comment');
    $comment = str_replace("<!--","",$comment[0]);
    $comment = str_replace(" -->","",$comment);
    $comment = explode(',', $comment);
    
    //Getting the Building & Unit Info
    $apartment->building = (int)trim(str_replace("buildingId:","",$comment[1]));
    $apartment->unit = trim(str_replace("unitId:","",$comment[2]));
    
    //Checking if building is a townhouse
    if( !is_numeric( str_replace("buildingId:","",$comment[1]) ) ){ continue; }

    //Price
    $apartment->price =(int)trim(str_replace(",","",str_replace("$","",$apt->find("span.pricing",0)->text())));

    //Available Date
    $apartment->avail = date("Y-m-d",strtotime(trim( str_replace("Special Offer","", explode("Available", $apt->find("div.specs",0)->text()))[1] )));

    //Bed
    $apartment->bed = (float)trim( str_replace( "Bed","", explode( "/", $apt->find("div.specs p")[1]->text() )[0] ) );
    
    //Bath
    $apartment->bath = (float)trim( str_replace( "Bath","", explode( "/", $apt->find("div.specs p")[1]->text() )[1] ) );

    //Footage
    $apartment->size = (int)trim( str_replace( "sq.ft.","", explode( "/", $apt->find("div.specs p")[2]->text() )[0] ) );

    //Floor
    $apartment->floor = trim(str_replace("unitId:","",$comment[2]))[0];

    //Layout
    $apartment->layout = $apt->find("img.static",0)->alt;

    //LayoutImage
    $apartment->image = $apt->find("img.static",0)->src;

    //Features
    $features = $apt->find("p.amenity");
    $apartment->features = [];
    foreach($features as $feat){
        array_push($apartment->features, trim( $feat->text() ) );
    }


    array_push($apartments, $apartment);

    /**
     * Saving to Database
     */

}

//Update the Availabiltiy to None
$query = $conn->prepare('UPDATE `apartment` SET `available` = 0 '); $query->execute(); $query->close();

foreach($apartments as $apt){
    //Insert & Get Building ID
    $buildingId = getBuilding($conn, $apt->building);
    if( $buildingId == ''){ $buildingId = insertBuilding($conn, $apt->building); }

    //Insert & Get Layout ID
    $layoutId = getLayout($conn, $apt->layout);
    if( $layoutId == ''){ $layoutId = insertLayout($conn, $apt->layout, $apt->bed, $apt->bath, $apt->size, $apt->image); }

    //Insert Apartment
    $aptId = getApartment($conn, $apt->unit, $apt->building);
    if( $aptId == ''){
        $aptId = insertApartment($conn, $apt->unit, $apt->floor, $apt->avail, $buildingId, $layoutId);
    }else{
        setAvailable($conn, $aptId, $apt->avail);
    }
    
    
    //Remove Amenities
    removeAmenities($conn, $aptId);

    //Insert New Price
    addPrice($conn, $aptId, $apt->price);

    //Insert & Get Amenities
    foreach( $apt->features as $feat ){
        $featId = getFeature($conn, $feat);
        if( $featId == ''){ $featId = insertFeature($conn, $feat); }

        //Add Amenity
        addAmenity($conn, $aptId, $featId);
    }
}

function getBuilding($conn, $number){
    $query = $conn->prepare('SELECT id FROM building WHERE number = ?');
    $query->bind_param("i",$number);
    $query->execute();
    $query->bind_result($result);
    $query->fetch();
    $query->close();
    return $result;
}

function insertBuilding($conn, $number){
    $query = $conn->prepare('INSERT INTO building (number) VALUES ( ? )');
    $query->bind_param("i",$number);
    $query->execute();
    $query->close();
    return $conn->insert_id;
}

function getLayout($conn, $name){
    $query = $conn->prepare('SELECT id FROM layouts WHERE name = ?');
    $query->bind_param("s",$name);
    $query->execute();
    $query->bind_result($result);
    $query->fetch();
    $query->close();
    return $result;
}

function insertLayout($conn, $name, $bed, $bath, $size, $image){
    $query = $conn->prepare('INSERT INTO layouts (name, bed, bath, size, image) VALUES ( ?,?,?,?,? )');
    $query->bind_param("sidis",$name,$bed,$bath,$size,$image);
    $query->execute();
    $query->close();
    return $conn->insert_id;
}

function getFeature($conn, $feat){
    $query = $conn->prepare('SELECT id FROM amenities WHERE name = ?');
    $query->bind_param("s",$feat);
    $query->execute();
    $query->bind_result($result);
    $query->fetch();
    $query->close();
    return $result;
}

function insertFeature($conn, $feat){
    $query = $conn->prepare('INSERT INTO amenities (name) VALUES ( ? )');
    $query->bind_param("s",$feat);
    $query->execute();
    $query->close();
    return $conn->insert_id;
}

function getApartment($conn, $unit, $building){
    $query = $conn->prepare('SELECT apartment.id FROM apartment JOIN building ON apartment.buildingId = building.id WHERE unit = ? AND number = ?');
    $query->bind_param("ii",$unit,$building);
    $query->execute();
    $query->bind_result($result);
    $query->fetch();
    $query->close();
    return $result;
}

function insertApartment($conn, $unit, $floor, $avail, $buildingId, $layoutId){
    $available = 1;
    $query = $conn->prepare('INSERT INTO apartment (`unit`, `floor`, `avail_date`, `buildingId`, `layoutId`, `available`) VALUES ( ?,?,?,?,?,? )');
    $query->bind_param("sssiii",$unit,$floor,$avail,$buildingId,$layoutId,$available);
    $query->execute();
    $query->close();
    return $conn->insert_id;
}

function removeAmenities($conn, $unit){
    $query = $conn->prepare('DELETE FROM `apt_amen` WHERE `apt_id` = ?');
    $query->bind_param("i",$unit);
    $query->execute();
    $query->close();
}

function addAmenity($conn, $unit, $feat){
    $query = $conn->prepare('INSERT INTO apt_amen (amen_id, apt_id) VALUES ( ?,?) ');
    $query->bind_param("ii",$feat, $unit);
    $query->execute();
    $query->close();
}

function addPrice($conn, $unit, $price){
    $query = $conn->prepare('INSERT INTO price (apt_id, price) VALUES ( ?,?) ');
    $query->bind_param("ii",$unit, $price);
    $query->execute();
    $query->close();
}

function setAvailable($conn, $id, $avail){
    //Update the Availabiltiy
    $query = $conn->prepare('UPDATE `apartment` SET `available` = 1, `avail_date` = ? WHERE `id` = ?');
    $query->bind_param("si",$avail,$id);
    $query->execute();
    $query->close();
}
?>