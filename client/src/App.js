import React, { Component } from 'react';
import './App.css';
import axios from 'axios'
import { Checkbox, Switch } from '@blueprintjs/core'
import SortBy from './components/SortBy'
import { withCookies } from 'react-cookie'
import '../node_modules/@blueprintjs/core/lib/css/blueprint.css'
import SingleApartment from './components/SingleApartment';
import Loading from './components/Loading';

class App extends Component {
  state = {
    data: null,
    loading: true,
    floor: [true, true, true, true, true, true],
    layout: [true, true, true, true, true, true, true, true],
    sortBy: 'Layout',
    showApartment: false,
    showApartmentUnit: null,
  }

  handleShowApartment = (unit = null) => {
    this.setState({ showApartment: !this.state.showApartment, showApartmentUnit: unit })
  }

  handleFloor = (c) => {
    let { data, layout, floor } = this.state
    floor[c] = !floor[c]
    this.saveCookie()
    this.filterByFloor(data, layout, floor, this.state.layouts, this.state.floors)

  }

  handleLayout = (c) => {
    let { data, layout, floor } = this.state
    layout[c.index] = !layout[c.index]
    this.saveCookie()
    this.filterByFloor(data, layout, floor, this.state.layouts, this.state.floors)
  }
  
  filterByFloor = (data, layout, floor, layouts, floors) => {
    let tempApt = []
    for(let x = 0; x<floors.length; x++){
      if(floor[x]) { tempApt = tempApt.concat(data.apartments.filter( (a) => a.floor === floors[x] )) }
    }
    this.filterByLayout(tempApt, layout, floor, layouts)
  }
  
  filterByLayout = (data, layout, floor, layouts) => {
    let tempApt = []
    for(let x = 0; x<layouts.length; x++){
      if(layout[x]) { tempApt = tempApt.concat(data.filter( (a) => a.layout.name === layouts[x] )) }
    }
    this.setState({ apartments: tempApt, layout, floor })
  }

  handleSortBy = (sortBy) => {
    const { cookies } = this.props

    //Setting Expiration Date for 2 weeks
    const expiration = new Date()
    expiration.setDate(expiration.getDate()+14)

    cookies.set('sort', sortBy, { path:"/", expires: expiration })
    this.setState({sortBy})
  }

  saveCookie = () => {
    const { cookies } = this.props

    //Setting Expiration Date for 2 weeks
    const expiration = new Date()
    expiration.setDate(expiration.getDate()+14)

    cookies.set('floors', this.state.floor, { path:"/", expires: expiration })
    cookies.set('layouts', this.state.layout, { path:"/", expires: expiration })
  }


  componentWillMount(){
    axios.get("https://ah-api.techsetta.com/api.php")
      .then( response => {
        const { cookies } = this.props
        let layoutCookie = cookies.get("layouts")
        let floorCookie = cookies.get("floors")
        let sortCookie = cookies.get("sort")

        if(layoutCookie === undefined){
          layoutCookie = this.state.layout
          floorCookie =  this.state.floor
          sortCookie = this.state.sortBy
        }

        this.filterByFloor(response.data, layoutCookie, floorCookie, response.data.layouts, response.data.floors)



        this.setState({loading: false, data: response.data, layouts: response.data.layouts, floors: response.data.floors, layout: layoutCookie, floor: floorCookie, sortBy: sortCookie})
      })
      .catch( error => {
        console.log(error);
      })
  }
  render() {
    if(this.state.loading){
      return (<Loading />)
    }
    const { data, apartments, layouts, floors } = this.state

    const updateDate = new Date(data.lastUpdate)
    
    return (
      <div id="App">
        { this.state.showApartment &&
            <SingleApartment handleShowApartment={this.handleShowApartment} unit={this.state.showApartmentUnit} />
        }
        <header>
          <div id="header">
            <h1>Apartment Hunter</h1>
          </div>
        </header>
        <div id="filterWrapper">
          <h3>Floor</h3>
          <ul className="filter">
            {floors.map( (floor, index) => (
              <li className="filterItem" key={ `floorKey_${index}` }><Checkbox checked={this.state.floor[index]} onChange={ () => { this.handleFloor(index) } } >Floor {floor}</Checkbox></li>
            ))}
          </ul>
          <h3>Layouts</h3>
          <ul className="filter">
            {layouts.map( (layout, index) => (
              <li className="filterItem" key={"filter_layout_" + index}>
                <Checkbox large="true" checked={ this.state.layout[index] } onChange={ () => { this.handleLayout({index}) } }>
                  {layout}
                </Checkbox>
              </li>
            ))}
          </ul>
          <h3>Sort By</h3>
          <ul className="filter">
              <li className="filterItem"><Switch checked={( this.state.sortBy === 'Layout' ? true : false)} label="Layout" onChange={() => { this.handleSortBy('Layout') }} /></li>
              <li className="filterItem"><Switch checked={( this.state.sortBy === 'Floor' ? true : false)} label="Floor" onChange={() => { this.handleSortBy('Floor') }} /></li>
              <li className="filterItem"><Switch checked={( this.state.sortBy === 'Price' ? true : false)} label="Price" onChange={() => { this.handleSortBy('Price') }} /></li>
              <li className="filterItem"><Switch checked={( this.state.sortBy === 'AvailableDate' ? true : false)} label="Available Date" onChange={() => { this.handleSortBy("AvailableDate") }} /></li>
          </ul>
        </div>
        <div id="content">
          <div className="apartmentsWrapper">
              { <SortBy
                  apartments={apartments}
                  sortBy={this.state.sortBy}
                  handleShowApartment={this.handleShowApartment}
                  />}
          </div>
        </div>
        <div id="lastUpdate">
          Last Price Update: {(updateDate.getMonth() + 1) +'/' + updateDate.getDate() + '/' + updateDate.getFullYear() + ' @ ' + updateDate.getHours() + ':00'}
        </div>
      </div>
    );
  }
}

export default withCookies(App);