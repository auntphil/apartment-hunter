import React, { Component } from 'react'
import Apartment from './Apartment';

function formatDate(date){
    const newDate = new Date(date)
    return (newDate.getUTCMonth() + 1) +'/' + newDate.getUTCDate() + '/' + newDate.getUTCFullYear()    
}

function sortyByLayout(a,b){
    if(a.layout.size < b.layout.size)
      return -1
    if(a.layout.size > b.layout.size)
      return 1
    return 0
  }
  
  function sortByFloor(a,b){
    if(a.floor < b.floor)
      return -1
    if(a.floor > b.floor)
      return 1
    return 0
  }

  function sortByPrice(a,b){
    if(a.prices[0].amount < b.prices[0].amount)
      return -1
    if(a.prices[0].amount > b.prices[0].amount)
      return 1
    return 0
  }

  function sortByAvailableDate(a,b){
    if(a.availableDate < b.availableDate)
      return 1
    if(a.availableDate > b.availableDate)
      return -1
    return 0
  }

  function checkShowTitle( lastTitle, newTitle ){
    if( lastTitle !== newTitle ){
        return [ newTitle, true ]
    }else{
        return [ lastTitle, false ]
    }
  }

class SortBy extends Component {
    
    renderApartment = (apartment) => {
        return <Apartment a={apartment} key={apartment.building + '_' + apartment.unit} handleShowApartment={this.props.handleShowApartment} />
    }

    render(){
        const { apartments, sortBy } = this.props
        let lastTitle = "", showTitle = false

        let sortedApartments = []
        /*Sorting the apartments*/
        switch(this.props.sortBy){
            case 'Price':
                sortedApartments = apartments.sort(sortByFloor).sort(sortyByLayout).sort(sortByPrice)
                break
            case 'AvailableDate':
                sortedApartments = apartments.sort(sortByFloor).sort(sortyByLayout).sort(sortByAvailableDate)
                break
            case 'Floor':
                sortedApartments = apartments.sort(sortyByLayout).sort(sortByFloor)
                break
            default:
                sortedApartments = apartments.sort(sortByFloor).sort(sortyByLayout)
                break
        }

        switch(sortBy){
            case 'Price':
                return(
                    <>
                    {sortedApartments.map( (apartment, index) => {
                        if( apartment.floor === 0 ){ return "" }
                        return this.renderApartment(apartment)
                    })}
                    </>
                )
                case 'AvailableDate':
                return(
                    <>
                    {sortedApartments.map( (apartment, index) => {
                        if( apartment.floor === 0 ){ return "" }
                        
                        const result = checkShowTitle(lastTitle, apartment.availableDate)
                        lastTitle = result[0]
                        showTitle = result[1]
                        
                        return(
                            <React.Fragment key={index}>
                                { (showTitle) &&
                                    <h1 className="sectionTitle" key={"section_title_" + apartment.availableDate} >Available: {formatDate(lastTitle)}</h1>
                                }
                                {this.renderApartment(apartment)}
                            </React.Fragment>
                        )
                    })}
                    </>
                )
            case 'Floor':
                return(
                    <>
                    {sortedApartments.map( (apartment, index) => {
                        if( apartment.floor === 0 ){ return "" }
                        
                        const result = checkShowTitle(lastTitle, apartment.floor)
                        lastTitle = result[0]
                        showTitle = result[1]

                        return(
                            <React.Fragment key={index}>
                                { (showTitle) &&
                                    <h1 className="sectionTitle" key={"section_title_" + apartment.layout.name} >Floor {lastTitle}</h1>
                                }
                                {this.renderApartment(apartment)}
                            </React.Fragment>
                        )
                    })}
                    </>
                )
            default:
            return(
                    <>
                    {apartments.map( (apartment, index) => {
                        if( apartment.floor === 0 ){ return "" }

                        const result = checkShowTitle(lastTitle, apartment.layout.name)
                        lastTitle = result[0]
                        showTitle = result[1]

                        return(
                            <React.Fragment key={index}>
                                { (showTitle) &&
                                    <h1 className="sectionTitle" key={"section_title_" + apartment.layout.name} >{lastTitle}</h1>
                                }
                                {this.renderApartment(apartment)}
                            </React.Fragment>
                        )
                    })}
                    </>
                )
        }
    }
}

export default SortBy