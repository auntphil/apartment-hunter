import React, { Component } from 'react'
import { Icon } from '@blueprintjs/core'
import Loading from './Loading';
import axios from 'axios'
import DailyPriceChart from './DailyPriceChart';

function formatDate(date){
    const newDate = new Date(date)
    return  ` ${(newDate.getMonth() + 1)} / ${newDate.getDate()} / ${newDate.getFullYear()}`
}

function formatNumber(number){
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function getPriceIncreases(a){
    const prices = a.prices.reverse().filter( ( price, index ) => {
        if(index === 0){return true}
        return price.amount !== a.prices[index - 1].amount
    }).reverse()
    a.prices.reverse()
    return prices
}

class SingleApartment extends Component {

    state = {
        loading: true,
        unit: null,
        priceChanges: [],
    }

    componentWillMount(){
        const unit = this.props.unit

        axios.get(`https://ah-api.techsetta.com/api.php?type=single&unit=${unit}`)
        .then( response => {
            const priceChanges = getPriceIncreases(response.data)
            this.setState({loading: false, data: response.data, unit, priceChanges})
        })
        .catch( error => {
          console.log(error);
        })
    }

    render(){
        /*If loading*/
        if(this.state.loading){
            return(
                <div id="showApartmentBackground">
                    <div id="showApartmentWrapper" className="apartment">
                        <div className="closeBtn" onClick={this.props.handleShowApartment}><Icon icon="cross" iconSize={25} />Close</div>
                        <Loading />
                    </div>
                </div>
            )
        }

        const { unit, data, priceChanges } = this.state

        return(
            <div id="showApartmentBackground">
              <div id="showApartmentWrapper" className="apartment">
                <div className="closeBtn" onClick={this.props.handleShowApartment}><Icon icon="cross" iconSize={25} />Close</div>
                    <div className="topRightData">
                        <div className="price">
                            ${formatNumber(data.currentPrice)}
                            </div>
                        <div className="date">{formatDate(data.availableDate)}</div>
                    </div>
                    <h1>Unit {unit}</h1>
                    <div className="detailWrapper">
                        <div className="detailItem">
                            <div style={{fontWeight:'bold'}}>{data.layout.name}</div>
                            <div>Building: { data.building }</div>
                            <div>Floor: { data.floor }</div>
                            <div>{ data.layout.bed } Bed / { data.layout.bath } Bath</div>
                            <div>{ formatNumber(data.layout.size) } square feet</div>
                        </div>
                        <div className="detailItem">
                            <ul>
                                {data.amenities.map( (amenity, index) => (
                                    <li key={"amenity_" + data.unit + "_" + index}>{amenity}</li>
                                    ))}
                            </ul>
                        </div>
                        <DailyPriceChart prices={data.prices} />
                    </div>
                    <div className="priceInfo">
                        <h3>Price Changes</h3>
                        <ul>
                            {
                                priceChanges.map( change => {
                                    return(
                                        <li key={`priceChange_${change.timestamp}`}><strong>${formatNumber(change.amount)}</strong> {formatDate(change.timestamp)}</li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default SingleApartment