import React, { Component } from 'react'
import { Icon } from '@blueprintjs/core'
import DailyPriceChart from './DailyPriceChart';

function formatDate(date){
    const newDate = new Date(date)
    return (newDate.getUTCMonth() + 1) +'/' + newDate.getUTCDate() + '/' + newDate.getUTCFullYear()
}

function formatNumber(number){
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function findPriceAtMidnight(a){
    const previous =  a.dayBeforePrice

    
    if(previous === null){
        //No price found
        return "cube-add"
    }
    //Price Decreased
    if(previous > a.currentPrice){
        return "trending-down"
    }
    //Price Increased
    if(previous < a.currentPrice){
        return "trending-up"
    }
    //Price Stayed Constant
    if(previous === a.currentPrice){
        return "layout-linear"
    }
}

class Apartment extends Component {
    render(){
        const { a } = this.props
        if( a.unit === 0 && a.floor === 0 ){return""}
        const priceChange = findPriceAtMidnight(a)
        return(
            <div className="apartment" onClick={ () => this.props.handleShowApartment(a.unit)} >
                <DailyPriceChart prices={a.prices} />
                <div className="topRightData">
                    <div className="price">
                        <Icon icon={priceChange} iconSize="20" style={{marginRight: '5px'}} />
                        ${formatNumber(a.currentPrice)}
                        </div>
                    <div className="date">{formatDate(a.availableDate)}</div>
                </div>
                <h1>Unit {a.unit}</h1>
                <div className="detailWrapper">
                    <div className="detailItem">
                        <div>Floor: { a.floor }</div>
                        <div>{ a.layout.bed } Bed / { a.layout.bath } Bath</div>
                        <div>{ formatNumber(a.layout.size) } square feet</div>
                    </div>
                    <div className="detailItem">
                        <ul>
                            {a.amenities.map( (amenity, index) => (
                                <li key={"amenity_" + a.unit + "_" + index}>{amenity}</li>
                                ))}
                        </ul>

                    </div>
                </div>

                <h3 className="layout">{a.layout.name}</h3>
            </div>
        )
    }
}

export default Apartment