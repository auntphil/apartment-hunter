import React, { Component } from 'react'
import { VictoryArea } from 'victory'

class DailyPriceChart extends Component{
    render(){
        const data = []
        let highest = 0, lowest = 30000
        this.props.prices.reverse().map( p => {
            if(p.amount > highest){highest = p.amount}
            if(p.amount < lowest){lowest = p.amount}
            data.push({x: p.timestamp, y: p.amount})
            return ''
        })

        /*If there is only one data point, duplicate it so the graph will still show*/
        if(this.props.prices.length === 1){
            data.push(data[0])
        }

        //Expanding the Domain
        lowest = lowest - 25
        highest = highest + 100

        //Setting the List back to normal order
        this.props.prices.reverse()
        

        return(
            <div className="dailyPriceChart">
                <VictoryArea
                    data={ data }
                    domain={{y: [lowest, highest]}}
                    style={{data: {
                        fill: "#cccccc3b",
                        stroke: "#1c81bf59", strokeWidth: 1
                    }}}
                    padding={0}
                />
            </div>
        )
    }
}

export default DailyPriceChart