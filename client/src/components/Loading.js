import React, { Component } from 'react'
import { Spinner } from '@blueprintjs/core';

class Loading extends Component {
    render(){
        return(
            <div id="loadingWrapper">
                <h1><Spinner />Loading</h1>
            </div>
        )
    }
}

export default Loading